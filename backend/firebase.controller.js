var request = require("request");

module.exports = {
	server_key: "AAAA-1k6Kyo:APA91bEikc1bhTG2lxlsh63Ow_esxSD6D_-nIwDALR5M5EQerO0jfTbuWa-CfzNrVSjirrnk5yXHQxg1uJxUUaZVRH8S7KwO5jKFZmoFp86TTQdOHqLAhsBj5Uv7EKyxiA6nAfMekrzVeE50m37vOvKwFOeDfU_Axg",
	registerToTopic: function(token, topic, onSuccess, onFail){
		var options = {
			method: 'POST',
			url: " https://iid.googleapis.com/iid/v1/"+token+"/rel/topics/"+topic,
			headers: {
				"content-type": "application/json",
				"authorization": "key=" + this.server_key
			}
		};
		request(options, function (error, response, body) {
			if (error){
				onFail(error);
			}
			else{
				if(response.statusCode === 200){
					onSuccess();
				} else {
					onFail(response.statusMessage);
				}
			}
		});
	},
	sendNotificationToTopic: function(payLoad, topic, onSuccess, onFail){
		var options = {
			method: "POST",
			url: "https://fcm.googleapis.com/fcm/send",
			headers: {
				"content-type": 'application/json',
				"authorization": "key=" + this.server_key
			},
			body: { 
				notification: payLoad,
				to: "/topics/" + topic
			},
			json: true 
		};

		request(options, function (error, response, body) {
			if (error) {
				onFail(error);
			}else{
				onSuccess(body);
			}
		});
	},
	sendNotificationToIndividualDevice: function(payLoad, token, onSuccess, onFail){
		var options = {
			method: "POST",
			url: "https://fcm.googleapis.com/fcm/send",
			headers: { 
				"content-type": 'application/json',
				"authorization": "key=" + this.server_key 
			},
			body: { 
				notification: payLoad,
				to: token 
			},
			json: true 
		};
		request(options, function (error, response, body) {
			if (error) {
				onFail(error);
			}else{
				onSuccess(body);
			}
		});
	}
};