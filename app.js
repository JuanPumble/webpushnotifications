var express    = require('express');
var app        = express();
var controller = require("./backend/firebase.controller.js");

var bodyParser = require('body-parser');
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
}));
app.use(express.static('public'));

// RUTAS
app.post('/register', function (req, res) {
	var result = controller.registerToTopic(req.body.token, req.body.topic, function(result){
		res.json({
			msg: result
		});
		res.end();
	}, function(error){
		res.statusCode = 500;
		res.json({
			error: error
		});
		res.end();
	});
});
app.post('/sendNotificationToTopic', function (req, res) {
	var result = controller.sendNotificationToTopic(req.body.payLoad, req.body.topic, function(result){
		res.json({
			msg: result
		});
		res.end();
	}, function(error){
		res.statusCode = 500;
		res.json({
			error: error
		});
		res.end();
	});
});
app.post('/sendNotificationToIndividualDevice', function (req, res) {
	var result = controller.sendNotificationToIndividualDevice(req.body.payLoad, req.body.token, function(result){
		res.json({
			msg: result
		});
		res.end();
	}, function(error){
		res.statusCode = 500;
		res.json({
			error: error
		});
		res.end();
	});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});