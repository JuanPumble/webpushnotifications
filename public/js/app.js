/* REFERENCIAS: 
 * https://developers.google.com/web/updates/2015/03/push-notifications-on-the-open-web
 * https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
 * https://developer.mozilla.org/es/docs/Web/API/Push_API
 * https://firebase.google.com/docs/cloud-messaging/server#implementing-http-connection-server-protocol
 * 
 * HACER NOTIFICACIONES BONITAS: 
 * https://developers.google.com/web/fundamentals/engage-and-retain/push-notifications/
 * https://developers.google.com/web/updates/2016/03/notifications
 *
 * COOKBOOK
 * https://serviceworke.rs/push-payload_server_doc.html
 * 
 * STACKOVERFLOW LO CONFIRMA, SIN PAYLOAD Y CON ENCRIPTACION PARA EL PAYLOAD
 * http://stackoverflow.com/questions/30335749/sending-data-payload-to-the-google-chrome-push-notification-with-javascript
 */
var pushNotifications = false;
var _serviceWorkerRegistration = null;

function ajaxSimple(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            callback(this);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function sendSubscriptionToServer(endpoint, key, authSecret){

    fetch('/registerForWebPushNotifications', {
        method: 'post',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            endpoint: endpoint,
            key: key,
            authSecret: authSecret,
        }),
    }).then(function(response){
        console.log(response);
    }).catch(function(error){
        console.error(error);
    });

    /*

    var data = JSON.parse(JSON.stringify(subscription));
    $.post("/postSubscription", data, function(data, textStatus, jqXHR){
        console.log("Exito");
        console.debug(data, textStatus);
    }, "json");
    */
    /*
    ajaxSimple(encodeURI("postSubscription?endPoint="+ subscription), function(result){
        console.debug(result);
    });
    */
}

function initializeState(serviceWorkerRegistration) {
    // ESTAN LAS NOTIFICACIONE SOPORTADAS POR EL SERVICE WORKER
    if (!('showNotification' in Object.getPrototypeOf(serviceWorkerRegistration))) {
        console.log("Las notificaciones no estan soportadas en el service worker");
        return;
    }
    _serviceWorkerRegistration = serviceWorkerRegistration;

    // REVISAR EL PERMISO DE NOTIFICACIONES ACTUAL 
    if (Notification.permission === 'denied') {
        console.log("EL USUARIO TIENE LAS NOTIFICACIONES DENEGADAS ACTUALMENTE.");
        return;
    }
    // REVISAR SI LAS NOTIFIACIONES PUSH ESTAN SOPORTADAS POR EL NAVEGADOR
    if (!('PushManager' in window)) {
        console.log("Las notificaciones push no estan soportadas en este navegador.");
        return;
    }
    /* AHORA QUE TODO ESTA SOPORTADO, NECESITAMOS REVISAR LAS SUBSCRIPCION A
     * LAS NOTIFICACIONES
     */

    serviceWorkerRegistration.pushManager.getSubscription().then(function(subscription) {
        /* HABILITAR EL BOTON PARA SUBSCRIBIRSE O DESINSCRIBIRSE DE
         * LAS NOTIFICIONES PUSH  
         */
        var btnSubscribe   = document.getElementById("btnSubscribe");
        var btnUnSubscribe = document.getElementById("btnUnSubscribe");

        if (!subscription) {
            // NO ESTAMOS SUBSCRITOS, PODEMOS HABILITAR LA OPCION DE SUBSCRIBIRSE 
            btnSubscribe.disabled = false;
            return;
        } else {
            // YA ESTAMOS SUBSCRITOS, PODEMOS DESUSCRIBIRNOS
            btnUnSubscribe.disabled = false;
            // SINCRONIZAR CON EL BACKEND
            sendSubscriptionToServer(subscription);
        }
    }).catch(function(err) {
        console.log("Error durante la subscripcion", err);
    });
}

function subscribe() {
    // DESABILITAR EL BOTON MIENTRAS SE PROCESA EL PERMISO
    var btnSubscribe   = document.getElementById("btnSubscribe");
    btnSubscribe.disabled = true;

    _serviceWorkerRegistration.pushManager.subscribe({
        userVisibleOnly: true
    }).then(function(subscription) {
        // LA SUBSCRIPCION SALIO EXITOSA 
        btnSubscribe.disabled = false;

        var rawKey        = subscription.getKey ? subscription.getKey('p256dh') : '';
        var key           = rawKey ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawKey))) : '';
        var rawAuthSecret = subscription.getKey ? subscription.getKey('auth') : '';
        var authSecret    = rawAuthSecret ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawAuthSecret))) : '';

        // ENVIAR AL SERVER LA SUBSCRIPCION PARA GUARDAR EL endPoint
        return sendSubscriptionToServer(subscription.endpoint, key, authSecret);
    })
    .catch(function(e) {
        if (Notification.permission === 'denied') {
            // The user denied the notification permission which  
            // means we failed to subscribe and the user will need  
            // to manually change the notification permission to  
            // subscribe to push messages  
            console.warn("Permiso para las notifiaciones denegado");
            btnSubscribe.disabled = true;
        } else {
            // A problem occurred with the subscription; common reasons  
            // include network errors, and lacking gcm_sender_id and/or  
            // gcm_user_visible_only in the manifest.  
            console.error("Imposible subscribirse a las notificaciones", e);
            btnSubscribe.disabled = false;
        }
    });
}

window.addEventListener('load', function() {
    /*
    // REVISAR SI EL NAVEGADOR SOPORTA EL SERVICE WORKER
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/js/service-worker.js').then(initializeState);
    } else {
        console.warn("El service worker no esta soportado por el navegador");
    }
    */
});