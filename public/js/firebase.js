/* REFERENCIA
 * https://www.youtube.com/watch?v=BsCBCudx58g&feature=youtu.be
 */
// Initialize Firebase
var config = {
	apiKey: "AIzaSyCv4-D0ORua3aofJhnCPYnzBldzNO4_k2A",
	authDomain: "testpushwebnotifications.firebaseapp.com",
	databaseURL: "https://testpushwebnotifications.firebaseio.com",
	storageBucket: "testpushwebnotifications.appspot.com",
	messagingSenderId: "1079533775658"
};
firebase.initializeApp(config);

// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

messaging.requestPermission().then(function() {
	debug("Permiso para notificaciones concedido");
	return messaging.getToken();
}).then(function(token){
	// AQUI HAY QUE ENVIAR EL TOKEN A BD
	if(token){
		setLocalToken(token);
		sendTokenToServer(token, "defaultTopicWeb");
	}
	else{
		debug("No hay token");
	}
}).catch(function(err) {
	debug("No nos dieron permiso de notificar");
	debug(err);
});

messaging.onTokenRefresh(function() {
	messaging.getToken().then(function(refreshedToken) {
		var currentLocalToken = localStorage.getItem("pushTokenCRHoy");
		if(refreshedToken !== currentLocalToken){ // SON TOKENS DISTINTOS
			setLocalToken(refreshedToken);
			sendTokenToServer(refreshedToken, "defaultTopicWeb");
		}
	}).catch(function(err) {
		debug("No se puede obtener un token actualizado");
		debug(err);
	});
});

messaging.onMessage(function(payload) {
	showNotification(payload.notification);
});

function setLocalToken(token){
	localStorage.setItem("pushTokenCRHoy", token);
}

function sendTokenToServer(token, topic){
	$.ajax({
		url: "/register",
		method: "POST",
		data: {
			token: token,
			topic: topic
		}
	}).done(function (response) {
		console.log(response);
		debug(response);
	});
}

function showNotification(payLoad){
	var notification = new Notification(payLoad.title, {
		icon: "img/icon_128x128.png",
		vibrate: [150, 100, 150, 125, 200],
		tag: payLoad.tag ? payLoad.tag : "",
		body: payLoad.body ? payLoad.body : "Haga clic aquí para ver esta noticia"
	});
	notification.onclick = function () {
        var win = window.open(payLoad.click_action, '_blank');
        win.focus();
    };
}

function sendNotificationToIndividualDevice(){
	$.ajax({
		url: "/sendNotificationToIndividualDevice",
		method: "POST",
		data: {
			token: localStorage.getItem("pushTokenCRHoy"),
			payLoad: {
    			title: "Notificacion para mi",
    			body: "Hola Pumble"
			}
		}
	}).done(function (response) {
		console.log(response);
		debug(response);
	});
}

function sendNotificationToTopic(){
	$.ajax({
		url: "/sendNotificationToTopic",
		method: "POST",
		data: {
			topic: "defaultTopicWeb",
			payLoad: {
    			title: "Notificacion sobre un tema especial",
    			body: "hola notificacion tematica"
			}
		}
	}).done(function (response) {
		console.log(response);
		debug(response);
	});
}

function debug(pVar){
	var parent = document.getElementById("debug");
	var msg = document.createElement("p");
	msg.innerHTML = JSON.stringify(pVar);
	parent.appendChild(msg);
}
function clearDebug(){
	document.getElementById("debug").innerHTML = "";
}