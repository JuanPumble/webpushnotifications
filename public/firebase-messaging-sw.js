importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');

// Initialize Firebase
var config = {
	apiKey: "AIzaSyCv4-D0ORua3aofJhnCPYnzBldzNO4_k2A",
	authDomain: "testpushwebnotifications.firebaseapp.com",
	databaseURL: "https://testpushwebnotifications.firebaseio.com",
	storageBucket: "testpushwebnotifications.appspot.com",
	messagingSenderId: "1079533775658"
};
firebase.initializeApp(config);

// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
	self.registration.showNotification(payload.title, {  
		icon: "http://localhost:3000/img/icon_128x128.png", 
		vibrate: [150, 100, 150, 125, 200],
		tag: payLoad.tag !== null ? payLoad.tag : "",
		body: payLoad.body !== null ? payLoad.body : "Haga clic aquí para ver esta noticia"
	});
});